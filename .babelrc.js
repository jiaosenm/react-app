// babel

const plugins = [];

plugins.push(['import', { libraryName: 'antd-mobile', style: 'css' }]);

if (process.env.NODE_ENV === 'production') {
	plugins.push('transform-remove-console');
}

module.exports = {
	presets: ['react-app'],
	plugins,
};
