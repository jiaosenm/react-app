## 宏烨找房

### 项目开发时遇到的问题

##### 一、 better-scroll 组件开发问题 (已解决)

    项目中多次出现上拉加载业务逻辑，把better-scroll封装成一个公共的的组件能减少重复的代码，使开发更轻松，但是开发时，遇到了很多问题。
    1. 在满足better-scroll滚动原理（父级盒子固定宽高 + 一个超出父级宽高的子级）及实例后（new better-scroll）都无法达到滚动效果。

    解决方法：在我们使用better-scroll时，要检测DOM变化，所以在实例时要加上 observeDOM: true(检测DOM变化) 以检测DOM变化，从而实现滚动。

    2. 组件和页面之间的事件传递，项目中涉及到上拉加载，要开启better-scroll中的pullUpLoad属性，同时要传递方法，多次尝试传递都出现只触发一次的问题。

    解决方法：在使用上拉加载(pullUpLoad)和下拉刷新(pullDownRefresh)每次更新之前都需要先把之前的事件清除，不然会累加。要监听页面数据变化，页面的数据要传给组件，useEffect监听这个DOM的变化，当变化时，执行off、once来清除事件，事件要延时执行结束行为（finishPullUp()、finishPullDown()），最后就是（refresh()：重新计算 BetterScroll，当 DOM 结构发生变化的时候务必要调用确保滚动的效果正常）

#### 二、form 组件的封装(已解决)

#### 三、顾问排行榜页面 tab 动态切换样式(已解决：通过 useRef 获取 DOM 实例，动态传递样式)

#### 三、输入文本的时候 手机的键盘会压缩浏览器的长度 并不是覆盖到浏览器 (未解决)
