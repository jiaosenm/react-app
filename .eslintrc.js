// ESlint
module.exports = {
	extends: ['react-app', 'react-app/jest', 'prettier', 'plugin:prettier/recommended'],
	root: true, // 在根路径下开发校验
	env: {
		node: true, // 环境
	},
	parserOptions: {
		// 解析代码格式
		parser: 'babel-eslint',
		ecmaFeatures: {
			jsx: true,
		},
	},
	plugins: ['prettier'],
	rules: {
		// 自定义校验规则
		'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
	},
};
