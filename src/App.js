import Router from '@/router/Router';
import routerConfig from '@/router/routerConfig.js';
import { NavLink } from 'react-router-dom';

const App = () => {
	return (
		<>
			<Router config={routerConfig} />
		</>
	);
};

export default App;
