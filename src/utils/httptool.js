import axios from 'axios';
import { Toast } from 'antd-mobile';
import code from './httpCode';

const Axios = axios.create({
	// baseURL: process.env.REACT_APP_BASEURL,
	baseURL: process.env.NODE_ENV === 'production' ? 'http://47.102.145.189:8009' : '',
	// 请求时间，结束请求
	timeout: 10000,
});

// 添加请求拦截器
Axios.interceptors.request.use(
	function (config) {
		// 在发送请求之前做些什么
		return {
			...config,
			headers: {
				...config.headers,
				Authorization: localStorage.getItem('token') || '',
			},
		};
	},
	function (error) {
		// 对请求错误做些什么
		return Promise.reject(error);
	},
);

// 添加响应拦截器
Axios.interceptors.response.use(
	function (response) {
		// 对响应数据做点什么

		const { data } = response;
		if (data.status === 200 || data.status === 3 || data.status === undefined) {
			return Promise.resolve(data);
		} else {
			Toast.fail(data.msg);
			return Promise.reject(data);
		}
	},
	function (error) {
		// 对响应错误做点什么

		switch (error.code) {
			case 'ECONNABORTED':
				Toast.fail('您的请求已超时，请重新获取数据');
				break;

			default:
				break;
		}

		code[error.code] && Toast.fail(code[error.code]);
		return Promise.reject(error);
	},
);

export default Axios;
