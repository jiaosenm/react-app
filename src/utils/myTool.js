import { useCallback, useRef, useEffect } from 'react';

export function useDebounce(fn, delay, dep = []) {
	const { current } = useRef({ fn, timer: null });
	useEffect(
		function () {
			current.fn = fn;
		},
		[fn],
	);

	return useCallback(function f(...args) {
		if (current.timer) {
			clearTimeout(current.timer);
		}
		current.timer = setTimeout(() => {
			current.fn.call(this, ...args);
		}, delay);
	}, dep);
}

// 防抖
export function debounce(func, delay = 300, immediate = false) {
	let timeout;
	return function () {
		clearTimeout(timeout);

		let _this = this;
		let args = arguments;
		if (immediate) {
			let callNow = !timeout;
			timeout = setTimeout(() => {
				timeout = null;
			}, delay);

			if (callNow) func.apply(_this, args);
		} else {
			// 立即执行
			timeout = setTimeout(function () {
				// 改变this指向, e
				func.apply(_this, args);
			}, delay);
		}
	};
}
