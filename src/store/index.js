import { createContext, useReducer, useContext } from 'react';
import reducer from './reducer';

const storeContext = createContext(null);

export const Provider = ({ children, store }) => {
	const initState = {
		num: 0,
		list: [],
	};
	const [state, dispatch] = useReducer(reducer, initState);

	return <storeContext.Provider value={{ state, dispatch }}>{children}</storeContext.Provider>;
};

export const useStore = () => {
	const { state, dispatch } = useContext(storeContext);

	return {
		state,
		dispatch,
	};
};
