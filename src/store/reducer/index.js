import { ADD, GET_LIST } from '../type';

const reducer = (state, action) => {
	switch (action.type) {
		case ADD:
			return { ...state, num: state.num + 1 };

		case GET_LIST:
			return { ...state, list: action.list };

		default:
			return { ...state };
	}
};

export default reducer;
