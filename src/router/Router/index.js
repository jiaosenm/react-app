import { HashRouter, BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { Suspense } from 'react';
// import AuthComponent from 'components/AuthComponent';

export const RouterView = ({ routes }) => {
	// console.log(routes);
	const list = routes.filter(item => !item.to);
	const redirect = routes.filter(item => item.to)[0];
	return (
		<Suspense fallback={<div>loading...</div>}>
			<Switch>
				{list.map(item => (
					<Route
						key={item.name}
						path={item.path}
						render={props => {
							// 设置页面title
							document.title = item.meta && item.meta.title;
							// return item.isAuth ? (
							// 	<AuthComponent {...props} component={item.component} />
							// ) : (
							// 	<item.component {...props} routes={item.routes || []} meta={item.meta} />
							// );
							return typeof item.beforeEnter === 'function' ? (
								item.beforeEnter(path => {
									if (path) {
										return <Redirect to={path} />;
									}
									return <item.component {...props} routes={item.routes || []} meta={item.meta} />;
								})
							) : (
								<item.component {...props} routes={item.routes || []} meta={item.meta} />
							);
						}}
					/>
				))}
				{redirect ? <Redirect to={redirect.to} from={redirect.form} /> : null}
			</Switch>
		</Suspense>
	);
};

const Router = ({ config }) => {
	// console.log(config);
	let { mode, routes } = Object.assign({}, { mode: 'hash', routes: [] }, config);

	return mode === 'hash' ? (
		<HashRouter>
			<RouterView routes={routes} />
		</HashRouter>
	) : (
		<BrowserRouter>
			<RouterView routes={routes} />
		</BrowserRouter>
	);
};

export default Router;
