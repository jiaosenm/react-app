import { lazy } from 'react';

const config = {
	mode: 'history',
	routes: [
		{
			to: '/index',
			from: '/',
		},
		{
			path: '/index',
			name: 'Index',
			component: lazy(() => import('@/pages/Index')),
			routes: [
				{
					to: '/index/home',
					from: '/index',
				},
				{
					path: '/index/home',
					name: 'Home',
					meta: {
						title: '首页',
					},
					component: lazy(() => import('@/pages/Index/Home')),
				},
				{
					path: '/index/seekHouse',
					name: 'SeekHouse',
					meta: {
						title: '找房',
					},
					component: lazy(() => import('@/pages/Index/SeekHouse')),
				},
				{
					path: '/index/news',
					name: 'News',
					meta: {
						title: '消息',
					},
					component: lazy(() => import('@/pages/Index/News')),
				},
				{
					path: '/index/publish',
					name: 'Publish',
					meta: {
						title: '发布',
					},
					component: lazy(() => import('@/pages/Index/Publish')),
				},
				{
					path: '/index/my',
					name: 'My',
					// isAuth: true,
					beforeEnter(next) {
						if (window.localStorage.getItem('token')) {
							return next();
						}
						return next('/login');
					},
					meta: {
						title: '我的',
					},
					component: lazy(() => import('@/pages/Index/My')),
				},
			],
		},
		{
			path: '/login',
			name: 'Login',
			meta: {
				title: '登录',
			},
			component: lazy(() => import('@/pages/Login')),
		},
		{
			path: '/publishDetail',
			name: 'PublishDetail',
			meta: {
				title: '发布详情',
			},
			component: lazy(() => import('@/pages/PublishDetail')),
		},
		{
			path: '/consultant',
			name: 'Consultant',
			meta: {
				title: '顾问排行榜',
			},
			component: lazy(() => import('@/pages/Consultant')),
		},
		{
			path: '/consultantDetail',
			name: 'ConsultantDetail',
			meta: {
				title: '顾问排行榜',
			},
			component: lazy(() => import('@/pages/ConsultantDetail')),
		},
		{
			path: '/addImage',
			name: 'AddImage',
			meta: {
				title: '添加图片',
			},
			component: lazy(() => import('@/pages/AddImage')),
		},
		{
			path: '/login',
			name: 'Login',
			meta: {
				title: '登录',
			},
			component: lazy(() => import('@/pages/Login')),
		},
		{
			path: '/collect',
			name: 'collect',
			meta: {
				title: '消息',
			},
			component: lazy(() => import('@/pages/My/collect')),
		},
		{
			path: '/Broker',
			name: 'broker',
			meta: {
				title: '经纪人',
			},
			component: lazy(() => import('@/pages/Broker')),
		},
		{
			path: '/Consulting',
			name: 'consulting',
			meta: {
				title: '咨询',
			},
			component: lazy(() => import('@/pages/Consulting')),
		},
	],
};

// 页面一级
export const list = [
	{
		to: '/index/home',
		icon: 'icon-index',
		name: '首页',
	},
	{
		to: '/index/seekHouse',
		icon: 'icon-fangyuan',
		name: '找房',
	},
	{
		to: '/index/publish',
		icon: 'icon-fabu',
		name: '发布',
	},
	{
		to: '/index/news',
		icon: 'icon-tubiao313',
		name: '消息',
	},
	{
		to: '/index/my',
		icon: 'icon-lianhe4',
		name: '我的',
	},
];

export default config;
