import React from 'react';
import ReactDOM from 'react-dom';
import FancyComponent from './components/FancyComponent';
import Router from '@/router/Router';
import routerConfig from '@/router/routerConfig.js';
import { Provider } from './store';
import './index.scss';
import 'lib-flexible';
import VConsole from 'vconsole';

if (process.env.NODE_ENV === 'development') {
	new VConsole();
}

ReactDOM.render(
	// <React.StrictMode>
	<FancyComponent>
		<Provider>
			<Router config={routerConfig} />
		</Provider>
	</FancyComponent>,
	// </React.StrictMode>,
	document.getElementById('root'),
);
