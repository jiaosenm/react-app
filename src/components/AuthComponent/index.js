// 路由守卫

import { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';

const AuthComponent = ({ component, ...rest }) => {
	const [isLogin, setIsLogin] = useState(false || (window.localStorage.getItem('token') ? true : false));

	useEffect(() => {
		if (window.localStorage.getItem('token')) {
			console.log('123');
			setIsLogin(true);
		}
		console.log(isLogin);
	}, []);

	return isLogin ? <component {...rest} /> : <Redirect to='/login' />;
};

export default AuthComponent;
