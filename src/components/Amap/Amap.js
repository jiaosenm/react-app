import React, { Component } from 'react';
import { Map, Marker, InfoWindow } from 'react-amap';
const mapKey = 'b1b0d55d776ca7b823031c9ba33d7675'; //需要自己去高德官网上去申请
class Address extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	render() {
		return (
			<div style={{ width: '100%', height: '400px' }}>
				<Map amapkey={mapKey} zoom={15} plugins={['ToolBar', 'Scale']}>
					<Marker position={['lng', 'lat']}></Marker>
				</Map>
			</div>
		);
	}
}

export default Address;
