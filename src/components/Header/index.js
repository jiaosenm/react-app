import style from './index.module.scss';
import classNames from 'classnames';
import { withRouter } from 'react-router-dom';
import { Icon } from 'antd-mobile';

const Header = props => {
	const { title, isGoBack = true, color = '', history } = props;
	return (
		<div className={classNames(style.header, color ? style.text : '')} style={{ backgroundColor: color }}>
			<span>{isGoBack ? <Icon type='left' size='md' onClick={() => history.goBack()} /> : props.children}</span>
			<p>{title}</p>
			<span></span>
		</div>
	);
};

export default withRouter(Header);
