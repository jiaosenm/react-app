import { Swiper, SwiperSlide } from 'swiper/react';
import style from './swiper.module.scss';
import classnames from 'classnames';
import SwiperCore, { Autoplay, Pagination } from 'swiper';

import 'swiper/swiper.scss';
import 'swiper/components/pagination/pagination.scss';

SwiperCore.use([Autoplay, Pagination]);

const ReactSwiper = ({ banner }) => {
	return (
		<Swiper className={classnames(style.classnames)} spaceBetween={50} autoplay loop pagination={{ clickable: true }}>
			{banner.length > 0 &&
				banner.map(item => {
					return (
						<SwiperSlide key={item.id}>
							<img src={'http://47.102.145.189:8009' + item.imgSrc} alt='' />
						</SwiperSlide>
					);
				})}
		</Swiper>
	);
};

export default ReactSwiper;
