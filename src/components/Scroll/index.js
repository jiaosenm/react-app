// better-scroll 组件

import { useRef, useEffect } from 'react';
import BScroll from 'better-scroll';
import styles from './index.module.scss';
import classNames from 'classnames';

const Scroll = props => {
	const wrapper = useRef(null);
	let bscroll = useRef(null);
	const initRef = useRef(false);

	const { direction, onPullUp, onPulldown, prop } = props;

	useEffect(() => {
		// better-scroll的配置参数
		bscroll.current = new BScroll(wrapper.current, {
			// 竖滚
			scrollY: direction === 'vertical',
			// 横滚
			scrollX: direction === 'horizontal',
			//probeType 为 3，任何时候都派发 scroll 事件，包括调用 scrollTo 或者触发 momentum 滚动动画
			probeType: 3,
			// 可以使用原生的点击
			click: true,
			// 检测dom变化
			observeDOM: true,
			// 上拉加载
			pullUpLoad: {
				threshold: 80,
				stop: 10,
			},
			// 下拉刷新
			pullDownRefresh: {
				threshold: 80,
				stop: 0,
			},
			// 过度动画, 在下载更多的时候滚动条会有个过度动画
			useTransition: true,
			//  显示滚动条
			scrollbar: true,
		});
		return () => {
			// 组件卸载时记得将其销毁
			bscroll.current.destroy();
		};
	}, []);

	// 上拉加载
	const pullUp = async () => {
		onPullUp && (await onPullUp());
		setTimeout(() => {
			bscroll.current.finishPullUp();
			bscroll.current.refresh();
		}, 500);
	};

	// 下拉刷新
	const pulldown = async () => {
		onPulldown && (await onPulldown());

		setTimeout(() => {
			//  记得使用finishPullDown，不然你只能下拉一次
			bscroll.current.finishPullDown();
			//  下拉之后你的content会发生变化，如果不使用refresh，你需要滑动一下才能刷新content的高度
			bscroll.current.refresh();
		}, 500);
	};

	useEffect(() => {
		if (initRef.current === true) {
			// 上拉加载
			//  每次更新都需要先把之前的pullingUp事件清除，不然会累加
			bscroll.current.off('pullingUp');
			bscroll.current.once('pullingUp', pullUp);

			// 下拉加载
			//  每次更新都需要先把之前的pullingDown事件清除，不然会累加
			bscroll.current.off('pullingDown');
			bscroll.current.once('pullingDown', pulldown);
		} else {
			initRef.current = true;
		}
		//  为什么监听prop是因为这边监听不到外面的state变化
		//  handlePullUp的[...state, ...res.data]中的state会中始终为一开始的[]
	}, [prop]);

	const PullUpdisplayStyle = true ? { display: '' } : { display: 'none' };
	const PullDowndisplayStyle = false ? { display: '' } : { display: 'none' };

	return (
		<div className={classNames(styles.wrapper)} ref={wrapper}>
			<div className={classNames(styles.content)}>
				{props.children}
				{/* 滑到底部加载动画 */}
				<div className={classNames(styles.pull_up)} style={PullUpdisplayStyle}>
					<div>loading...</div>
				</div>
				{/* 顶部下拉刷新动画 */}
				<div style={PullDowndisplayStyle}>
					<div>数据加载完毕</div>
				</div>
			</div>
		</div>
	);
};

export default Scroll;

// 调用
// 需要传滚动方向 direction='vertical(竖)' 'horizontal(横)'
// onPullUp、onPulldown 父组件
// // 上拉加载
// const handlePullUp = useCallback(() => {
// 	publish.consultant(obj).then(res => {
// 		console.log(res);
// 		setList(list.concat(res.data));
// 	});
// }, [list]);
// // 下拉刷新
// const handlePullDown = useCallback(() => {
// 	publish.consultant(obj).then(res => {
// 		console.log(res);
// 		setList(res.data);
// 	});
// });
