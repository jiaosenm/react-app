import { Component } from 'react';
import * as Sentry from '@sentry/react';
import { Integrations } from '@sentry/tracing';

Sentry.init({
	dsn: 'https://68d771d49feb45f19733ec92f463f26d@o959899.ingest.sentry.io/5908829',
	integrations: [new Integrations.BrowserTracing()],
	environment: process.env.NODE_ENV,
	// Set tracesSampleRate to 1.0 to capture 100%
	// of transactions for performance monitoring.
	// We recommend adjusting this value in production
	tracesSampleRate: 1.0,
});

export default class FancyComponent extends Component {
	componentDidCatch(error) {
		Sentry.captureException(error);
	}

	render() {
		return this.props.children;
	}
}
