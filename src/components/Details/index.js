import style from './index.module.scss';
import classNames from 'classnames';
import { Icon, Picker, List } from 'antd-mobile';

const PublishDetail = props => {
	console.log(props);

	let { columns, changeVal, selectChange, data } = props;

	return (
		<div className={classNames(style.list)}>
			{columns.map(item => {
				return !item.slot ? (
					<div className={classNames(style.list_item)} key={item.label}>
						<div className={classNames(style.item_left)}>
							<p>{item.label}</p>
							{/* {item.name ? (
								<div>
									{item.type === 'select' ? (
										<Picker data={item.data || []} cols={1} className='forss' onOk={val => selectChange(val, item.title)}>
											<div>{data[item.title]}</div>
										</Picker>
									) : (
										<input type='text' defaultValue='' onChange={e => changeVal(item.title, e)} />
									)}
								</div>
							) : ( */}
							<div className={classNames(item.name ? '' : style.active)}>
								{item.type === 'select' ? (
									<Picker data={item.data || []} cols={1} className='forss' onOk={val => selectChange(val, item.title)}>
										<div>{data[item.title] ? data[item.title] : '请选择'}</div>
									</Picker>
								) : (
									<input type='text' defaultValue='' placeholder='请输入' onChange={e => changeVal(item.title, e)} />
								)}
							</div>
							{/* )} */}
						</div>
						<div className={classNames(style.item_right)}>{item.depict ? <div>{item.depict}</div> : <Icon type='right' size='md' />}</div>
					</div>
				) : (
					props.children
				);
			})}
		</div>

		// 上传图片
		// <div className={classNames(style.upload)}>
		//     <div className={classNames(style.left)}>+</div>
		//     <div className={classNames(style.center)}>
		//         <div>上传图片</div>
		//         <p>只能上传房屋图片,不能含有文字、数字、网址、名片、水印等，所有类别图片总计20张。</p>
		//     </div>
		//     <Icon type="right" size='md' />
		// </div>
	);
};

export default PublishDetail;
