import { useState, useEffect, useContext, isValidElement, cloneElement } from 'react';
import { FormContext } from './index';
import { Icon } from 'antd-mobile';

const getValue = e => {
	return e.target && e.target.type === 'file' ? e.target.file : e.target.value;
};

const FormItem = ({ name, label, children }) => {
	const store = useContext(FormContext);

	const [value, setValue] = useState(() => name && store.get(name));

	let newChildren = children;

	useEffect(() => {
		// 设置一个监听器
		return store.$on((n, value) => {
			n && name === n && setValue(value);
		});
	}, [name, store]);

	if (isValidElement(newChildren)) {
		newChildren = cloneElement(newChildren, {
			value,
			onChange(e) {
				store.set(name, getValue(e));
			},
		});
	}

	return (
		<div>
			{label && (
				<div>
					<label>{label}</label>
				</div>
			)}
			{newChildren}
			<Icon type='right' size='md' />
		</div>
	);
};

export default FormItem;
