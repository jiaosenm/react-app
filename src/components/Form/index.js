// form表单
import { createContext } from 'react';
export { default as FormItem } from './FormItem';
export const FormContext = createContext(null);

const Form = ({ onSubmit = () => {}, children, store }) => {
	const handleSubmit = e => {
		e.preventDefault();
		onSubmit(e);
	};

	return (
		<FormContext.Provider value={store}>
			<form onSubmit={handleSubmit}>{children}</form>
		</FormContext.Provider>
	);
};

class FormStore {
	constructor(values, rules) {
		this.defaultValues = JSON.parse(JSON.stringify(values));
		this.values = JSON.parse(JSON.stringify(values));
		this.rules = rules;
		this.events = [];
	}

	$on(ck) {
		this.events.push(ck);
		return () => {
			const index = this.events.indexOf(ck);
			index > -1 && this.events.splice(index, 1);
		};
	}

	get(name) {
		return name ? this.values[name] : this.values;
	}

	set(name, val) {
		if (name && name === '*') {
			this.values = JSON.parse(JSON.stringify(this.defaultValues));
			// 触发监听器
			this.events.forEach(ck => {
				Object.keys(this.values).forEach(key => {
					ck(key, this.values[key]);
				});
			});
		} else if (name && val !== undefined) {
			this.values[name] = val;
			// 触发监听器
			this.events.forEach(ck => ck(name, val));
			console.log('asfd', this.values);
		}
	}

	reset() {
		this.set('*');
	}
}

export const createFormStore = (values = {}, rules = {}) => {
	return new FormStore(values, rules);
};

export default Form;

// 调用
// import Form, { FormItem, createFormStore } from 'components/Form';

// const News = props => {
// 	console.log(props);
// 	// const {} = props;
// 	const FormStore = createFormStore({
// 		area: '123',
// 		title: '标题',
// 		via: '',
// 	});

// 	const onSubmit = () => {
// 		console.log(FormStore.get());
// 		FormStore.reset();
// 	};

// 	return (
// 		<div>
// 			<Form store={FormStore} onSubmit={onSubmit}>
// 				<FormItem label='小区' name='area'>
// 					<input type='text' />
// 				</FormItem>
// 				<FormItem label='选择' name='title'>
// 					<select>
// 						<option value='xx'>xx</option>
// 						<option value='xxx'>xxx</option>
// 					</select>
// 				</FormItem>

// 				<button type='submit'>提交</button>
// 			</Form>
// 		</div>
// 	);
// };

// export default News;
