import './style.scss';
import { useHistory } from 'react-router-dom';
import { broker1 } from '@/assets/data/broker1';

const Broker = () => {
	//点击箭头返回
	const history = useHistory();
	const goBack = () => {
		history.go(-1);
	};
	const list = broker1;
	const titleList = list.list.slice(0, 3);
	const mainList = list.list.slice(3);
	console.log(list.list, 'list');
	console.log(titleList, 'titleList');
	console.log(mainList, 'mainList');
	return (
		<div className='BrokerBox'>
			<header>
				<span onClick={goBack}>
					<svg t='1629458991286' className='icon' viewBox='0 0 1024 1024' version='1.1' xmlns='http://www.w3.org/2000/svg' p-id='2057' width='23' height='23'>
						<path
							d='M346.15033867 512.36233264l430.31418735-430.32155185c18.08422786-18.07244469 18.08422787-47.4066589-1e-8-65.48057651-18.07244469-18.08422786-47.4066589-18.08422787-65.48057648 0l-463.05963077 463.06257654c-18.08864653 18.07244469-18.08864655 47.4066589-1e-8 65.4805765l463.06110366 463.06110367c9.03916814 9.04358683 20.89009642 13.56685314 32.73955182 13.56685314s23.70185657-4.52326631 32.73955179-13.56685315c18.08864653-18.07244469 18.08864655-47.4066589 0-65.48057649l-430.31713316-430.32302477z'
							p-id='2058'
							fill='#ffffff'></path>
					</svg>
				</span>
				<div>经纪人</div>
			</header>
			<main>
				<div className='searchBox'>
					<i>
						<svg t='1629460627882' className='icon' viewBox='0 0 1024 1024' version='1.1' xmlns='http://www.w3.org/2000/svg' p-id='2073' width='18' height='18'>
							<path
								d='M943.552 864.448l-90.368-90.368c-19.904-19.904-23.744-50.816-9.28-74.944 38.464-64.064 60.544-139.072 59.776-219.392C901.504 251.456 715.648 65.6 486.848 64c-233.6-1.6-424.512 189.248-422.912 422.912 1.6 228.8 187.456 414.656 415.808 416.832 80.32 0.768 155.328-21.248 219.328-59.776 24.128-14.528 55.04-10.688 74.944 9.216l90.368 90.368c21.888 21.888 57.344 21.888 79.168 0 21.824-21.824 21.824-57.216 0-79.104zM550.08 784.896C328.576 831.04 136.704 639.104 182.848 417.664 207.36 299.904 299.84 207.424 417.6 182.912c221.504-46.144 413.376 145.792 367.232 367.232-24.512 117.76-116.992 210.24-234.752 234.752z'
								p-id='2074'
								fill='#dbdbdb'></path>
						</svg>
					</i>
					<input type='text' placeholder='请输入需要查询的经纪人' />
				</div>
				<div className='imgBox'>
					<img src='https://images.tengfangyun.com/images/new_icon/adviser_list_bg1.png?x-oss-process=style/w_8601' alt='' />
				</div>
				<div className='colorBox'>
					<div className='listBox'>
						<div className='left_box'>
							<div className='leftCon_Box'>
								<div className='img_Box'>
									<img
										src='https://images.sqfcw.com/attachment/information/20210630/229289db398ea9a150530070498987119423d70c.jpeg?x-oss-process=style/w_120'
										alt=''
									/>
								</div>
								<div className='bc_img'>
									<img src='https://images.tengfangyun.com/images/new_icon/No2.png' alt='' />
								</div>
								<span>朱中原</span>
							</div>
						</div>
						<div className='mid_box'>
							<div className='minCon_Box'>
								<div className='img_Box'>
									<img
										src='https://images.sqfcw.com/attachment/information/20201113/0500bccd7d87865f580d07893d08c9bf83320c39.jpeg?x-oss-process=style/w_120'
										alt=''
									/>
								</div>
								<div className='bc_img'>
									<img src='https://images.tengfangyun.com/images/new_icon/No1.png' alt='' />
								</div>
								<span>陈冬红</span>
							</div>
						</div>
						<div className='right_box'>
							<div className='rightCon_Box'>
								<div className='img_Box'>
									<img
										src='https://images.sqfcw.com/attachment/information/20201204/f56558c6a2de646c2ca757c90e408cc9d5f5cbf0.jpeg?x-oss-process=style/w_120'
										alt=''
									/>
								</div>
								<div className='bc_img'>
									<img src='https://images.tengfangyun.com/images/new_icon/No3.png' alt='' />
								</div>
								<span>侍美丽</span>
							</div>
						</div>
					</div>
				</div>
				<div className='mainBox'>
					{mainList.map((item, index) => {
						return (
							<div className='item_box' key={index}>
								<div className='item_con'>
									<div className='leftBox'>
										<span className='numSpan'>04</span>
										<div className='imgBox'>
											<img src={item.img} />
										</div>
										<div className='nameBox'>
											<span>{item.cname}</span>
											<p>{item.tname}</p>
										</div>
									</div>
									<div className='rightBox'>
										<i>
											<svg
												t='1629960390484'
												className='icon'
												viewBox='0 0 1024 1024'
												version='1.1'
												xmlns='http://www.w3.org/2000/svg'
												p-id='8214'
												width='27'
												height='27'>
												<path
													d='M512 842.666667a388.48 388.48 0 0 1-143.146667-26.88 32 32 0 1 1 23.466667-59.52 330.24 330.24 0 0 0 119.68 21.333333c173.013333 0 313.813333-133.973333 313.813333-298.666667S685.013333 181.333333 512 181.333333s-313.813333 133.973333-313.813333 298.666667a287.36 287.36 0 0 0 42.666666 149.333333 32 32 0 0 1-54.826666 34.133334 351.36 351.36 0 0 1-51.84-183.466667c0-199.893333 169.386667-362.666667 377.813333-362.666667s377.813333 162.773333 377.813333 362.666667-169.386667 362.666667-377.813333 362.666667z'
													fill='#fb656a'
													p-id='8215'></path>
												<path
													d='M213.333333 906.666667a31.786667 31.786667 0 0 1-16.426666-4.48A32.426667 32.426667 0 0 1 181.333333 874.666667V646.613333a32 32 0 0 1 64 0v174.933334l120.32-64a32 32 0 0 1 29.866667 56.746666l-167.253333 88.746667a33.493333 33.493333 0 0 1-14.933334 3.626667z'
													fill='#fb656a'
													p-id='8216'></path>
												<path
													d='M512 454.826667m-42.666667 0a42.666667 42.666667 0 1 0 85.333334 0 42.666667 42.666667 0 1 0-85.333334 0Z'
													fill='#fb656a'
													p-id='8217'></path>
												<path
													d='M353.066667 454.826667m-42.666667 0a42.666667 42.666667 0 1 0 85.333333 0 42.666667 42.666667 0 1 0-85.333333 0Z'
													fill='#fb656a'
													p-id='8218'></path>
												<path
													d='M673.92 454.826667m-42.666667 0a42.666667 42.666667 0 1 0 85.333334 0 42.666667 42.666667 0 1 0-85.333334 0Z'
													fill='#fb656a'
													p-id='8219'></path>
												<path
													d='M391.466667 755.84h-21.76l-3.2 1.28a33.493333 33.493333 0 0 0-15.786667 17.066667 32.213333 32.213333 0 0 0 18.133333 42.666666l14.506667 4.906667 12.16-6.4a32.213333 32.213333 0 0 0-4.053333-58.453333zM229.973333 673.92a31.36 31.36 0 0 0 14.933334-30.72v-1.706667l-1.28-4.906666v-1.066667a54.613333 54.613333 0 0 0-2.346667-5.12 32 32 0 0 0-59.52 16.213333v8.746667l4.693333 8.106667a31.786667 31.786667 0 0 0 43.52 10.453333z'
													fill='#fb656a'
													p-id='8220'></path>
											</svg>
										</i>
										<b>
											<svg
												t='1629960487071'
												className='icon'
												viewBox='0 0 1024 1024'
												version='1.1'
												xmlns='http://www.w3.org/2000/svg'
												p-id='9075'
												width='23'
												height='23'>
												<path
													d='M614.22 699.934c1.746-1.084 20.9-13.07 26.503-16.444 10.36-6.265 18.914-10.963 27.467-14.818 60.115-27.347 113.122-8.011 183.236 73.909 43.37 50.718 56.2 95.714 39.454 135.77-12.469 29.937-36.503 50.598-79.27 76.137-2.77 1.627-27.407 15.963-33.731 19.878-99.57 61.44-327.8-79.21-491.942-319.548C121.374 413.877 78.787 156.973 179.682 94.63l13.312-8.433L207.45 76.8c51.02-33.009 83.667-46.2 122.398-39.635 38.672 6.626 71.56 35.238 99.389 87.341 58.669 110.23 45.718 162.515-29.757 210.04-5.42 3.494-24.696 15.12-26.503 16.264-18.492 11.445 12.83 88.125 85.655 194.741C532.36 653.372 593.98 712.343 614.159 699.934z'
													fill='#fb656a'
													p-id='9076'></path>
											</svg>
										</b>
									</div>
								</div>
							</div>
						);
					})}
				</div>
			</main>
		</div>
	);
};

export default Broker;
