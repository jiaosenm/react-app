import classNames from 'classnames';
import { useState, useRef, useEffect } from 'react';
import style from './index.module.scss';
import Header from 'components/Header';
import { scroller, Element } from 'react-scroll';
import { Toast } from 'antd-mobile';
import { useHistory } from 'react-router-dom';
import io from 'socket.io-client';

const img = 'https://cube.elemecdn.com/6/94/4d3ea53c084bad6931a56d5158a48jpeg.jpeg';
const Consulting = props => {
	const [val, setVal] = useState('');
	const [flag, setFlag] = useState(false);
	const scrollId = useRef(null);
	const SocketItem = useRef(null);
	const history = useHistory();
	const [list, setList] = useState([]);
	const [userinfo, setUserInfo] = useState({});

	useEffect(() => {
		// 客服id
		const id = props.location.search.split('?')[1].split('=')[1];
		console.log(id);
		// 链接服务器
		SocketItem.current = io('http://192.168.43.219:8081/robot');
		// 验证身份
		SocketItem.current.emit('authenticate', {
			token: window.localStorage.getItem('token'),
		});
		// 链接
		SocketItem.current.on('connected', data => {
			if (data.code === 401) {
				Toast.fail(data.msg, 1, () => {
					history.replace('/login');
				});
				return;
			}
			setUserInfo(data.userinfo);
			setList(val => [
				...val,
				{
					id: 1,
					value: data.questionList,
				},
			]);
		});
		SocketItem.current.on('message', data => {
			console.log(data);
			setList(val => [
				...val,
				{
					id: 1,
					value: data.data,
				},
			]);
		});
	}, []);

	const handleClickQuestion = ({ question, id }) => {
		SocketItem.current.emit('message', { id });
		setList(val => [
			...val,
			{
				id: 2,
				value: question,
			},
		]);
	};
	const handleSend = () => {
		SocketItem.current.emit('message', { type: 'talk', value: val });
		setList(valitem => [
			...valitem,
			{
				id: 2,
				value: val,
			},
		]);
	};
	return (
		<div className={classNames(style.consulting)}>
			<Header title='咨询' />
			<div ref={scrollId} className={classNames(style.main)}>
				<div className={classNames(style.main_scroll)}>
					{/* 客服id=1 , 用户id=2 */}
					{list.map(item => {
						return item.id === 1 ? (
							<div className={classNames(style.nav)}>
								<img src={img} alt='' />
								<span>
									{Array.isArray(item.value)
										? item.value.map(item => (
												<b key={item.id} onClick={() => handleClickQuestion(item)}>
													{item.question}
												</b>
										  ))
										: item.value}
								</span>
							</div>
						) : (
							<div className={classNames(style.nav, style.nav_right)}>
								<span>{item.value}</span>
								<img src={userinfo?.avatar} alt='' />
							</div>
						);
					})}
					<Element name='buttonElement'></Element>
				</div>
			</div>

			{/* 底部 */}
			<div className={classNames(style.footer)}>
				{flag ? (
					<div className={classNames(style.foot_left)}>
						<i className='iconfont icon-yuyin' onClick={() => setFlag(!flag)}></i>
						<span className={classNames(style.div_span)}>长按发送语言</span>
					</div>
				) : (
					<div className={classNames(style.foot_left)}>
						<i className='iconfont icon-jianpan' onClick={() => setFlag(!flag)}></i>
						<div className={classNames(style.div_input)}>
							<input
								type='text'
								value={val}
								onChange={e => {
									setVal(e.target.value);
								}}
							/>
						</div>
					</div>
				)}
				{val ? (
					<div className={classNames(style.div_right)}>
						<i className='iconfont icon-biaoqing'></i>
						<span onClick={handleSend}>发送</span>
					</div>
				) : (
					<div className={classNames(style.div_right)}>
						<i className='iconfont icon-biaoqing'></i>
						<i className='iconfont icon-jjia-'></i>
					</div>
				)}
			</div>
		</div>
	);
};

export default Consulting;
