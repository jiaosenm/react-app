import Header from 'components/Header';

const News = props => {
	console.log(props);
	// const {} = props;

	return (
		<div>
			<Header title='消息' isGoBack={false}>
				<div style={{ marginLeft: '0.5rem' }}>
					<i className='iconfont icon-shanchu'></i>
					<span>清除未读</span>
				</div>
			</Header>
		</div>
	);
};

export default News;
