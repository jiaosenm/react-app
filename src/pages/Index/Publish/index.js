import classNames from 'classnames';
import style from './publish.module.scss';
import { useHistory } from 'react-router-dom';

const list = [
	{
		title: '二手车出售',
		url: 'https://images.tengfangyun.com/images/icon/ershoufang.png?x-oss-process=style/w_220',
		text: '发布二手车出售信息',
	},
	{
		title: '有房出租',
		url: 'https://images.tengfangyun.com/images/icon/zufang.png?x-oss-process=style/w_220',
		text: '发布房源出租信息',
	},
	{
		title: '我想买个房子',
		url: 'https://images.tengfangyun.com/images/icon/qiugou.png?x-oss-process=style/w_220',
		text: '发布求购意信息',
	},
	{
		title: '我想租个房子',
		url: 'https://images.tengfangyun.com/images/icon/qiuzu.png?x-oss-process=style/w_220',
		text: '发布求租意向信息',
	},
	{
		title: '买新房 职业顾问帮您忙',
		url: 'https://images.tengfangyun.com/images/icon/xinfang.png?x-oss-process=style/w_220',
		text: '提交购房意向买房顾问全程带看',
	},
];

const Publish = () => {
	const history = useHistory();

	const goPage = title => {
		if (title === '买新房 职业顾问帮您忙') {
			history.push('/consultant');
		} else {
			history.push({
				pathname: '/publishDetail',
			});
		}
	};

	return (
		<div className={classNames(style.publish)}>
			<div className={classNames(style.nav)}>发布</div>

			<ul className={classNames(style.list)}>
				{list.map(item => {
					return (
						<li key={item.title} className={classNames(style.list_li)} onClick={() => goPage(item.title)}>
							<div className={classNames(style.icon)}>
								{/* <i className={'iconfont ' + item.icon}></i> */}
								<img src={item.url} alt='' />
							</div>
							<div className={classNames(style.li_center)}>
								<h4>{item.title}</h4>
								<span>{item.text}</span>
							</div>
							<span className={classNames(style.gt)}>&gt;</span>
						</li>
					);
				})}
			</ul>
		</div>
	);
};

export default Publish;
