import Swiper from 'components/Swiper';
import React, { useEffect, useState } from 'react';
import api from '@/api';
import classNames from 'classnames';
import style from './home.module.scss';
// import Map from "../../components/Amap/Amap"
import { navList, custom } from '@/assets/data/data';
import { Carousel } from 'antd-mobile';

const { home } = api;

const Home = () => {
	const [list, setList] = useState([]);
	useEffect(() => {
		home.banner().then(res => {
			if (res.status === 200) {
				setList(res.body);
			}
		});
	}, []);
	const list2 = custom.news.news;
	const list2Item = list2?.map((item, index) => {
		return <div className={classNames(style.list2Item)}>{item.title}</div>;
	});
	const newsList2View = [];
	//2个为1组显示;
	for (let i = 0; i < list2Item.length; i += 2) {
		const view = list2Item?.slice(i, i + 2);
		let key = 0;
		while (view?.length < 2) {
			view.push(<div className={classNames(style.list2Item)} key={`j-${key}`}></div>);
			key += 1;
		}
		newsList2View.push(<React.Fragment key={i}>{view}</React.Fragment>);
	}
	return (
		<div className={classNames(style.home)}>
			<Swiper banner={list} />
			<div className={classNames(style.nav)}>
				{navList.map((item, index) => {
					return (
						<div key={item.title}>
							<div className={classNames(style.imgss)}>
								<img src={item.image} />
							</div>
							<div className={classNames(style.title)}>
								<span>{item.title}</span>
							</div>
						</div>
					);
				})}
			</div>
			<div className={classNames(style.othTitle)}>
				<div className={classNames(style.tt)}>
					<div className={classNames(style.ofc)}>
						<span className={classNames(style.left)}>{custom.benyuefangjia.title}</span>
						<span className={classNames(style.right)}>{custom.loushibao.title}</span>
					</div>
					<div className={classNames(style.pm)}>
						<div className={classNames(style.xtop)}>
							<div className={classNames(style.xleft)}>
								<div className={classNames(style.title)}>
									<span className={classNames(style.price)}>{custom.benyuefangjia.left.value}</span>
									<span> 元/m²</span>
								</div>
								<div className={classNames(style.text)}>
									<span>{custom.benyuefangjia.left.desc}</span>
								</div>
							</div>
							<div className={classNames(style.xright)}>
								<div className={classNames(style.title)}>
									<span className={classNames(style.price)}>{custom.benyuefangjia.right.value}</span>
									<span> 元/m²</span>
								</div>
								<div className={classNames(style.text)}>
									<span>{custom.benyuefangjia.right.desc}</span>
								</div>
							</div>
						</div>
						<div className={classNames(style.xbottom)}>
							<div className={classNames(style.bottomContent)}>
								<Carousel dotPosition='right' dots={false} autoplay>
									{newsList2View}
								</Carousel>
							</div>
						</div>
					</div>
					<div className={classNames(style.hotBuild)}>
						<div className={classNames(style.hotBuildTitle)}>
							<span>{custom.hotBuild.title}</span>
						</div>
						{custom.hotBuild.hotBuild.map(item => {
							return (
								<div className={classNames(style.hotBuildList)} key={item.title}>
									<div>
										<div className={classNames(style.leftImg)}>
											<img src={item.img} />
										</div>
										<div className={classNames(style.rightDetail)}>
											<div className={classNames(style.topTitle)}>
												<span>{item.group_title || item.title}</span>
												<div className={classNames(style.status)} style={{ backgroundColor: `${item.status_color}` }}>
													<span>{item.status_name}</span>
												</div>
											</div>
											<div className={classNames(style.price)}>
												{item.price ? (
													<div>
														<span className={classNames(style.sPrice)}>{item.price}</span>
														<span>{item.price_unit}</span>
													</div>
												) : (
													<div>
														<span className={classNames(style.sPrice)}>{item.build_price}</span>
													</div>
												)}
											</div>
											<div className={classNames(style.biuldType)}>
												{item.build_type?.map((item2, index2) => {
													return <span key={index2}>{item2}</span>;
												})}
											</div>
											<div className={classNames(style.discount)}></div>
										</div>
									</div>
								</div>
							);
						})}
					</div>
				</div>
			</div>
		</div>
	);
};
export default Home;
