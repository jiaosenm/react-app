import { RouterView } from '@/router/Router';
import { NavLink } from 'react-router-dom';
import { list } from '@/router/routerConfig';

const Index = ({ routes }) => {
	return (
		<>
			<div className='main'>
				<RouterView routes={routes} />
			</div>

			<div className='footer'>
				{list.map(item => {
					return (
						<NavLink to={item.to} key={item.name}>
							<i className={'iconfont ' + item.icon}></i>
							{item.name}
						</NavLink>
					);
				})}
			</div>
		</>
	);
};

export default Index;
