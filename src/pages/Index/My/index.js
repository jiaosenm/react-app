import vConsole from 'vconsole';
// import { useState } from 'react'
import style from './index.module.css';
import { Button } from 'antd-mobile';
import Bgd from '@/images/1.png'; //背景图
import handX from '@/images/hand.jpeg'; //头像
import { RightOutlined } from '@ant-design/icons';
import { createFromIconfontCN } from '@ant-design/icons'; //icon图标
if (process.env.NODE_ENV === 'development') {
	new vConsole();
}
const MyIcon = createFromIconfontCN({
	scriptUrl: '//at.alicdn.com/t/font_2559576_az3lu803477.js', // 在 iconfont.cn 上生成
});
var bgdStyle = {
	width: '100%',
	height: '185px',
	// makesure here is String确保这里是一个字符串，以下是es6写法
	backgroundImage: `url(${Bgd})`,
};
const My = props => {
	console.log(props);
	//跳转消息页面
	const news = () => {
		props.history.push('/collect');
	};
	//跳转收藏页面
	const collect = () => {
		props.history.push('/collect');
	};
	//跳转浏览历史
	const history = () => {
		props.history.push('/collect');
	};
	//跳转发布页面
	const issue = () => {
		props.history.push('/collect');
	};
	//跳转支付明细
	const detail = () => {
		props.history.push('/collect');
	};
	//跳转问题反馈
	const feedback = () => {
		props.history.push('/collect');
	};
	//跳转联系客服
	const kefu = () => {
		props.history.push('/collect');
	};
	return (
		<div className={style.my}>
			<main className={style.mymain}>
				<div className={style.box} style={bgdStyle}>
					<h3 className={style.h3}>个人中心</h3>
					<div className={style.boxs}>
						<dl className={style.top}>
							<dt>
								<img className={style.myimg} src={handX} alt='' />
							</dt>
							<dd>
								<h4>香蕉不拿呐</h4>
								<span>18037046248</span>
							</dd>
							<dd
								onClick={() => {
									news();
								}}>
								<MyIcon type='icon-xinfeng' className={style.svg} />
							</dd>
						</dl>
						<ul className={style.bottom}>
							<li
								onClick={() => {
									issue();
								}}
								className={style.li}>
								<MyIcon type='icon-wendang' className={style.svg} />
								<span>我的发布</span>
							</li>
							<li
								onClick={() => {
									collect();
								}}
								className={style.li}>
								<MyIcon type='icon-fabu-' className={style.svg} />
								<span>收藏</span>
							</li>
							<li
								onClick={() => {
									history();
								}}
								className={style.li}>
								<MyIcon type='icon-liulanlishi' className={style.svg} />
								<span>浏览历史</span>
							</li>
						</ul>
					</div>
				</div>
				<div className={style.list}>
					<div
						className={style.pay}
						onClick={() => {
							detail();
						}}>
						<span>支付明细</span>
						<RightOutlined />
					</div>
					<div
						className={style.issue}
						onClick={() => {
							feedback();
						}}>
						<span>问题反馈</span>
						<RightOutlined />
					</div>
					<div
						className={style.service}
						onClick={() => {
							kefu();
						}}>
						<span>联系客服</span>
						<RightOutlined />
					</div>
				</div>
			</main>
			<footer className={style.footer}>
				<Button type='primary' inline size='large'>
					退出登录
				</Button>
			</footer>
		</div>
	);
};

export default My;
