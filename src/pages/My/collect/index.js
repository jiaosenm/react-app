import React, { useEffect, useState } from 'react';

import style from '../collect/index.module.css';
import Header from 'components/Header'; //头部组件

const Collect = () => {
	const [list] = useState([]);
	useEffect(() => {}, []);

	return (
		<div className={style.collect}>
			<Header title={'我的收藏'} color='#1948F3' />
			<main className={style.main}>
				{list.map((item, index) => {
					return (
						<dl className={style.dl} key={index}>
							<dt className={style.dl}>
								<img src={item.img} alt='' />
							</dt>
							<dd className={item.dd}>
								<h3>{item.title}</h3>
								<span>{item.word}</span>
								<span className={style.yue}>{item.price}/月</span>
								<div className={style.xiaoqu}>小区</div>
							</dd>
						</dl>
					);
				})}
			</main>
		</div>
	);
};

export default Collect;
