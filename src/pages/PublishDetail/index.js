import Details from 'components/Details';
import style from './index.module.scss';
import classNames from 'classnames';
import { Icon, Button } from 'antd-mobile';
import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { columns } from './data';

const PublishDetail = props => {
	console.log(props);
	const [obj, setObj] = useState({
		// 小区
		community: '',
		// 类型
		type: '',
		// 区域
		area: '',
		// 上传图片
		imgUrl: '',
		// 户型
		unit: '',
		// 面积
		ha: '',
		// 租金
		price: '',
		// 标题
		text: '',
		// 描述
		description: '',
		tel: '',
	});
	const history = useHistory();

	useEffect(() => {
		const imgUrl = props.location.search ? props.location.search.split('?')[1].split('=')[1] : '';
		setObj({
			...obj,
			imgUrl,
		});
	}, []);

	// input
	const changeVal = (val, e) => {
		// console.log(val,e.target.value);
		setObj({
			...obj,
			[val]: e.target.value,
		});
		console.log(obj);
	};
	// select
	const selectChange = (val, name) => {
		console.log('选择', val[0], name);
		setObj({
			...obj,
			[name]: val[0],
		});
		console.log(obj);
	};
	// 发布
	const publishClick = () => {
		console.log(obj);
	};

	return (
		<div className={classNames(style.detail)}>
			<div className={classNames(style.header)}>
				<Icon type='left' size='md' onClick={() => history.push('/index/publish')} />
				<p>发布信息</p>
			</div>

			<div className={classNames(style.main)}>
				<Details columns={columns} data={obj} changeVal={changeVal} selectChange={selectChange}>
					{/* 上传图片 */}
					<div className={classNames(style.upload)} onClick={() => history.push('/addImage')}>
						<div className={classNames(style.left)}>{obj.imgUrl ? <img src={obj.imgUrl} alt='' /> : '+'}</div>
						<div className={classNames(style.center)}>
							<div>上传图片</div>
							<p>只能上传房屋图片,不能含有文字、数字、网址、名片、水印等，所有类别图片总计20张。</p>
						</div>
						<Icon type='right' size='md' />
					</div>
				</Details>
				<div className={classNames(style.time)}>有效时间</div>
				<div className={classNames(style.time)}>推广方式</div>
				<div className={classNames(style.check)}>
					<input type='checkbox' /> &nbsp;
					<p>
						已阅读并接受 <span>《宿迁房产网房源信息发布规则》</span>
					</p>
				</div>

				<div className={classNames(style.btn)}>
					<Button type='warning' onClick={publishClick}>
						立即发布
					</Button>
				</div>
			</div>
		</div>
	);
};

export default PublishDetail;
