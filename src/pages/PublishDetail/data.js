export const columns = [
	{
		label: '小区',
		type: 'select',
		name: '',
		title: 'community',
		data: [
			{
				label: '八维研修学院',
				value: '八维研修学院',
			},
			{
				label: '中关村',
				value: '中关村',
			},
		],
	},
	{
		label: '房产类型',
		name: '',
		type: 'select',
		title: 'type',
		data: [
			{
				label: '独栋/别墅',
				value: '独栋/别墅',
			},
			{
				label: '高级洋房',
				value: '高级洋房',
			},
		],
	},
	{
		label: '区域',
		name: '',
		type: 'select',
		title: 'area',
		data: [
			{
				label: '海淀区',
				value: '海淀区',
			},
			{
				label: '朝阳区',
				value: '朝阳区',
			},
		],
	},
	{
		label: '',
		title: '',
		slot: true,
	},
	{
		label: '面积',
		type: 'input',
		name: '',
		title: 'ha',
		depict: '平方',
	},
	{
		label: '售价',
		type: 'input',
		name: '',
		title: 'price',
		depict: '万元',
	},
	{
		label: '标题',
		name: '',
		type: 'select',
		title: 'text',
		data: [
			{
				label: '前靠山，背靠水',
				value: '前靠山，背靠水',
			},
			{
				label: '别墅就是舒服！',
				value: '别墅就是舒服！',
			},
		],
	},
	{
		label: '描述',
		type: 'input',
		name: '',
		title: 'description',
	},
	{
		label: '联系人',
		type: 'input',
		name: '',
		title: 'tel',
	},
];
