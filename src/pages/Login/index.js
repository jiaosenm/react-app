import Header from 'components/Header';
import style from './index.module.scss';
import classNames from 'classnames';
import { useState } from 'react';
import { Toast } from 'antd-mobile';
import { useHistory } from 'react-router-dom';
import api from '@/api';

const Login = props => {
	const { login } = api;
	const history = useHistory();
	const [form, setForm] = useState({
		username: '',
		password: '',
	});

	const changVal = (name, e) => {
		setForm({ ...form, [name]: e.target.value });
	};

	const handleLogin = () => {
		console.log(form);
		if (form.username || form.password) {
			login.login(form).then(res => {
				console.log(res);
				if (res.status === 200) {
					window.localStorage.setItem('token', res.body.token);
					history.push('/index/home');
				} else {
					Toast.fail('账号或密码有误！');
				}
			});
		} else {
			Toast.fail('账号和密码不能为空！');
		}
	};

	return (
		<div className={classNames(style.login)}>
			<Header isGoBack={false} title='登录' />

			<div className={classNames(style.main)}>
				<div className={classNames(style.img_box)}>
					<img
						src='https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F3725429809%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1632715397&t=5c007337b32df29fe65ce5832471c10d'
						alt=''
					/>
				</div>
				<div className={classNames(style.form)}>
					<input type='text' placeholder='用户名' value={form.username} onChange={e => changVal('username', e)} />
					<input type='text' placeholder='密码' value={form.password} onChange={e => changVal('password', e)} />
					<button onClick={handleLogin}>登录</button>
				</div>
			</div>
		</div>
	);
};

export default Login;
