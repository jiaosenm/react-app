import Header from 'components/Header';
import styles from './index.module.scss';
import classNames from 'classnames';
import { useEffect, useState } from 'react';
import { useStore } from '@/store';

const option = [
	{
		title: '立即转发',
		icon: 'icon-zhuanfa',
		rbg: '#00CAA7',
	},
	{
		title: '帮我点赞',
		icon: 'icon-dianzan1',
		rbg: '#FB656A',
	},
	{
		title: '生成海报',
		icon: 'icon-erweima',
		rbg: '#4CC7F6',
	},
];

const ConsultantDetail = props => {
	const id = props.location.search.split('?')[1].split('=')[1];
	const { state, dispatch } = useStore();
	const [item, setItem] = useState({});

	useEffect(() => {
		console.log(state.list);
		let item = state.list.filter(item => item.id == id)[0];
		setItem(item);
	}, []);

	return (
		<div className={classNames(styles.details)}>
			<Header color='#000' />

			<div className={classNames(styles.main)}>
				{/* 名片 */}
				<div className={classNames(styles.nav)}>
					<div className={classNames(styles.nav_top)}></div>
					<div className={classNames(styles.nav_center)}>
						<div className={classNames(styles.icon_img)}>
							<img src='https://images.sqfcw.com/images/new_icon/tongpai@3x.png?x-oss-process=style/w_240' alt='' />
						</div>
						<div className={classNames(styles.top)}>
							<div className={classNames(styles.top_img)}>
								<img src={item.prelogo} alt='' />
							</div>
							<div className={classNames(styles.top_center)}>
								<h4>
									{item.cname} <span>{item.level_name}</span>
								</h4>
								<span>主营楼盘：{item.build_names}</span>
							</div>
						</div>
						<div className={classNames(styles.center)}>
							<div className={classNames(styles.center_box)}>
								<span>浏览量</span>
								<b>{item.browse}</b>
							</div>
							<div className={classNames(styles.center_box)}>
								<span>咨询量</span>
								<b>{item.traffic_volume}</b>
							</div>
							<div className={classNames(styles.center_box)}>
								<span>点赞数</span>
								<b>{item.star}</b>
							</div>
						</div>
						<div className={classNames(styles.bottom)}>
							<p>个性签名：{item.minfo}</p>
							<h4>浏览量：{item.browse}</h4>
						</div>
					</div>
				</div>

				{/*  */}
				<div className={classNames(styles.goto)}>
					<ul className={classNames(styles.goto_con)}>
						{option.map(item => {
							return (
								<li>
									<p style={{ backgroundColor: item.rbg }}>
										<i className={'iconfont ' + item.icon}></i>
									</p>
									<span>{item.title}</span>
								</li>
							);
						})}
					</ul>
				</div>
			</div>
		</div>
	);
};

export default ConsultantDetail;
