import { Icon, ImagePicker, Button } from 'antd-mobile';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import style from './index.module.scss';
import classNames from 'classnames';

const AddImage = () => {
	const history = useHistory();
	const [files, setFiles] = useState([]);

	const changeImg = (files, type, index) => {
		// console.log(files, type, index);
		setFiles(files);

		console.log(files);
	};

	return (
		<div className={classNames(style.addimage)}>
			<div className={classNames(style.header)}>
				<Icon type='left' size='md' onClick={() => history.push('/publishDetail')} />
				<p>添加图片</p>
			</div>

			<div className={classNames(style.main)}>
				<div className={classNames(style.nav)}>
					<p>室内图/视频</p>
					<span>最多上传10张{files.length}/10</span>
				</div>

				<div className={classNames(style.img)}>
					<ImagePicker
						files={files}
						onChange={(files, type, index) => changeImg(files, type, index)}
						onImageClick={(index, fs) => console.log(index, fs)}
						selectable={files.length < 7}
						length='3'
					/>
				</div>
			</div>

			<div className={classNames(style.btn)}>
				<Button
					type='warning'
					onClick={() => {
						let url = files[0].url;
						history.push(`/publishDetail?url=${url}`);
					}}>
					完成
				</Button>
			</div>
		</div>
	);
};

export default AddImage;
