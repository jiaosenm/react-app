import { useEffect, useState, useRef, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { useStore } from '@/store';
import { SearchBar } from 'antd-mobile';
import Header from 'components/Header';
import Scroll from 'components/Scroll';
import classNames from 'classnames';
import style from './index.module.scss';
import api from '@/api';

const { publish } = api;

const tabList = [
	{
		title: '优选置业顾问',
		color: '#FB656A',
		img: 'https://images.tengfangyun.com/images/new_icon/adviser_list_bg1.png?x-oss-process=style/w_8601',
		type: 1,
	},
	{
		title: '咨询榜',
		color: '#00CAA7',
		img: 'https://images.tengfangyun.com/images/new_icon/adviser_list_bg2.png?x-oss-process=style/w_8601',
		type: 2,
	},
	{
		title: '新人榜',
		color: '#4CC7F6',
		img: 'https://images.tengfangyun.com/images/new_icon/adviser_list_bg3.png?x-oss-process=style/w_8601',
		type: 3,
	},
];

const iconList = [
	{
		icon: 'icon-xiaoxi',
	},
	{
		icon: 'icon-dianhua',
	},
];

const Consultant = () => {
	const history = useHistory();
	const { state, dispatch } = useStore();
	const [list, setList] = useState([]);
	const [curIndex, setIndex] = useState(0);
	const [obj, setObj] = useState({
		page: 1,
		rows: 10,
		type: 1,
		searchtxt: '',
	});
	const myNav = useRef();
	const myNav_top = useRef();

	const getList = async () => {
		let res = await publish.consultant(obj);
		// console.log(res);
		setList(res.data);
		dispatch({
			type: 'GET_LIST',
			list: res.data,
		});
	};

	useEffect(() => {
		getList();
	}, []);

	// tab切换
	const tabClick = (i, { color, img }) => {
		setIndex(i);
		myNav.current.style.backgroundColor = color;
		myNav_top.current.style.background = `url(${img}) center`;
		myNav_top.current.style.backgroundSize = '100%';
	};

	// 条详情
	const goDetails = id => {
		console.log(id);
		history.push(`/consultantDetail?id=${id}`);
	};

	// 点击消息、电话
	const handleClick = (e, i, id) => {
		e.stopPropagation();
		console.log(id);
		if (i === 0) {
			history.push(`/consulting?id=${id}`);
		} else {
		}
	};

	// 上拉加载
	const handlePullUp = useCallback(() => {
		console.log('上拉加载');
		publish.consultant(obj).then(res => {
			setList(list.concat(res.data));
		});
	}, [list]);

	// 下拉刷新
	const handlePullDown = useCallback(() => {
		console.log('下拉刷新');
		getList();
	});

	return (
		<div className={classNames(style.consultant)}>
			<Header title='顾问排行榜' />

			<div className={classNames(style.main)}>
				{/* 滚动 */}
				<Scroll direction='vertical' onPullUp={handlePullUp} onPulldown={handlePullDown} prop={list}>
					<div className={classNames(style.nav)} ref={myNav}>
						<div className={classNames(style.nav_search)}>
							<SearchBar placeholder='可按楼盘或顾问名称搜索' />
						</div>
						<div className={classNames(style.nav_top)} ref={myNav_top}></div>
						<div className={classNames(style.nav_post)}>
							{list.length > 0 &&
								list.map((item, index) => {
									return (
										index === 1 && (
											<div className={classNames(style.nav_post_item)} key={item.id} onClick={() => goDetails(item.id)}>
												<img src={item.prelogo} alt='' className={classNames(style.img1)} />
												<div className={classNames(style.img2)}>
													<img src='https://images.tengfangyun.com/images/new_icon/No2.png' alt='' />
												</div>
												<h4>{item.cname}</h4>
												<p>{item.build_names}</p>
											</div>
										)
									);
								})}
							{list.length > 0 &&
								list.map((item, index) => {
									return (
										index === 0 && (
											<div className={classNames(style.nav_post_item)} key={item.id} onClick={() => goDetails(item.id)}>
												<img src={item.prelogo} alt='' className={classNames(style.img1)} />
												<div className={classNames(style.img2)}>
													<img src='https://images.tengfangyun.com/images/new_icon/No1.png' alt='' />
												</div>
												<h4>{item.cname}</h4>
												<p>{item.build_names}</p>
											</div>
										)
									);
								})}
							{list.length > 0 &&
								list.map((item, index) => {
									return (
										index === 2 && (
											<div className={classNames(style.nav_post_item)} key={item.id} onClick={() => goDetails(item.id)}>
												<img src={item.prelogo} alt='' className={classNames(style.img1)} />
												<div className={classNames(style.img2)}>
													<img src='https://images.tengfangyun.com/images/new_icon/No3.png' alt='' />
												</div>
												<h4>{item.cname}</h4>
												<p>{item.build_names}</p>
											</div>
										)
									);
								})}
						</div>
					</div>

					{/* tab 切换 */}
					<div className={classNames(style.cate_box)}>
						<div className={classNames(style.cate_list)}>
							{tabList.map((item, index) => {
								return (
									<div
										key={item.title}
										className={classNames(style.cate_item, curIndex === index ? style.active : '')}
										style={{
											backgroundColor: curIndex === index ? item.color : '',
										}}
										onClick={() => tabClick(index, item)}>
										{item.title}
									</div>
								);
							})}
						</div>
					</div>

					{/* 列表 */}
					<div className={classNames(style.scroll)}>
						{list.length > 0 &&
							list.map((item, index) => {
								return (
									index > 2 && (
										<div key={index} className={classNames(style.scroll_item)} onClick={() => goDetails(item.id)}>
											<b>{index < 9 ? '0' + (index + 1) : index + 1}</b>
											<div className={classNames(style.item_center)}>
												<img src={item.prelogo} alt='' />
												<div className={classNames(style.center_con)}>
													<h3>{item.cname}</h3>
													<p>浏览量{item.browse}</p>
													<span>{item.build_names}</span>
												</div>
											</div>
											<div className={classNames(style.item_bottom)}>
												<div className={classNames(style.icon_box)}>
													{iconList.map((ite, ind) => {
														return (
															<div
																className={classNames(style.icon, style['rgba' + (curIndex + 1)])}
																key={ite.icon}
																onClick={e => handleClick(e, ind, item.id)}>
																{ind === 0 ? (
																	<i className={'iconfont ' + ite.icon}></i>
																) : (
																	<a href={'tel:' + ite.tel}>
																		<i className={'iconfont ' + ite.icon}></i>
																	</a>
																)}
															</div>
														);
													})}
												</div>
												<div className={classNames(style.like)}>
													<i className='iconfont icon-dianzan'></i>
													{item.star}
												</div>
											</div>
										</div>
									)
								);
							})}
					</div>
				</Scroll>
			</div>
		</div>
	);
};

export default Consultant;
