export const banner = {
	url: '/api/home/swiper',
	method: 'get',
};

export const news = {
	url: '/api/home/news',
	method: 'get',
};

export const groups = {
	url: '/api/home/groups',
	method: 'get',
};
