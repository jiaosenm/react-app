import axios from '@/utils/httptool';

const dirData = require.context('./config', true, /\.js$/);

const res = dirData.keys().reduce((val, item) => {
	const apis = dirData(item);
	const nameSpeace = item.match(/\/(\w+)\.js$/)[1];
	val[nameSpeace] = Object.keys(apis).reduce((val, key) => {
		val[key] = (data = {}) => {
			if (apis[key].method === 'post') {
				return axios({ ...apis[key], data });
			} else {
				return axios({
					url: apis[key].url.replace(/:(\w+)/g, ($1, $2) => {
						return data[$2];
					}),
					method: apis[key].method,
					params: data,
				});
			}
		};
		return val;
	}, {});
	return val;
}, {});

export default res;
