let navList = [
	{
		title: '新房',
		image: 'https://images.sqfcw.com/images/icon/icon3/ic_xinfang@3x.png?x-oss-process=style/w_240',
		navId: '1',
	},
	{
		title: '二手房',
		image: 'https://images.sqfcw.com/images/icon/icon3/ic_ershoufang@3x.png?x-oss-process=style/w_240',
		navId: '2',
	},
	{
		title: '租房',
		image: 'https://images.sqfcw.com/images/icon/icon3/ic_chuzu@3x.png?x-oss-process=style/w_240',
		navId: '3',
	},
	{
		title: '团购看房',
		image: 'https://images.sqfcw.com/images/icon/icon3/ic_kanfangtuan@3x.png?x-oss-process=style/w_240',
		navId: '4',
	},
	{
		title: '地图找房',
		image: 'https://images.sqfcw.com/images/icon/icon3/ic_map@3x.png?x-oss-process=style/w_240',
		navId: '5',
	},
	{
		title: '咨询',
		image: 'https://images.sqfcw.com/images/icon/icon3/ic_zixun@3x.png?x-oss-process=style/w_240',
		navId: '6',
	},
	{
		title: '楼市圈',
		image: 'https://images.sqfcw.com/images/icon/icon3/ic_loushiquan@3x.png?x-oss-process=style/w_240',
		navId: '7',
	},
	{
		title: '置业顾问',
		image: 'https://images.sqfcw.com/attachment/focus/20201212/d268ca1a31c9c8987375d9de65c38622dc9397f6.png?x-oss-process=style/w_240',
		navId: '8',
	},
	{
		title: '经纪人',
		image: 'https://images.sqfcw.com/images/icon/icon3/ic_jingjiren@3x.png?x-oss-process=style/w_240',
		navId: '9',
	},
	{
		title: '预售查询',
		image: 'https://images.sqfcw.com/images/icon/icon3/ic_yushouzheng@3x.png?x-oss-process=style/w_240',
		navId: '10',
	},
];

export default navList;
